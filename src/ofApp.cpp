#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  ofBackground(0);
  ofNoFill();
  ofSetCircleResolution(50);
  
  angle = 50;
  
  ofSetLineWidth(2);
  
  ofSetFrameRate(60);
  
  earthDist = 150;
  
}

//--------------------------------------------------------------
void ofApp::update(){
  angle += 0.2;
  earthDist--;
}

//--------------------------------------------------------------
void ofApp::draw(){
  ofPushMatrix();
  ofTranslate(ofGetWidth()/2,ofGetHeight()/2);
  ofRotateDeg(angle * 5);
  //Draw Sun
  ofDrawCircle(0,0,40);
  ofDrawLine(0,0,0,40);
  //Draw Earth
  ofPushMatrix();
  ofRotateDeg(angle);
  ofTranslate(earthDist,0);
  ofPushMatrix();
  ofRotateDeg(angle * 20);
  ofDrawCircle(0,0,12);
  ofDrawLine(0,0,0,12);
  ofPushMatrix();
  ofRotateDeg(angle);
  ofTranslate(25,0);
  ofPushMatrix();
  ofRotateDeg(angle * 4);
  ofDrawCircle(0,0,5);
  ofDrawLine(0,0,0,5);
  ofPopMatrix();
  ofPopMatrix();
  ofPopMatrix();
  ofPopMatrix();
  ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
